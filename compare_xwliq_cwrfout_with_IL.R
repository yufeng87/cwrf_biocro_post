#!/usr/bin/env Rscript
#args = commandArgs(trailingOnly=TRUE)
#if (length(args)==0) {
#  stop("please input the csspout folder name (not path!)", call.=FALSE)
#}
#print(args)
library(ncdf4)
library(R.matlab)
library(abind)
is.leapyear=function(year){
  #http://en.wikipedia.org/wiki/Leap_year
  return(((year %% 4 == 0) & (year %% 100 != 0)) | (year %% 400 == 0))
}
#csspout-198012.nc
xy=read.csv('../cssp_post/CWRF_coor_in_km.csv',header=TRUE,col.names = c('x','y'))
lat_km=xy$x
lon_km=xy$y[is.na(xy$y)==FALSE]
#get porosity
f=nc_open("~/gladework/cssp_inputs/wrfinput_d01_1.2")
xporsl=ncvar_get(f,"XPORSL")
lat   = ncvar_get(f,"XLAT")
lon   = ncvar_get(f,"XLONG")
nc_close(f)
#get dz
#f=nc_open("~/scratch/csspouts/out_clmwt_obPRAVG_yh_slpcut75/cssprst-198003.nc")
#dz=ncvar_get(f,"XDZ")
#nc_close(f)
#dz=dz[,,6:14]
#print(c("dz dimension is ",dim(dz)))
#site_names=c("BVL","ORR","DEK","CMI","DXS")
#ob_coors=c(40.0529, -88.3715)
#x1=c(39.8058,-90.8238)
#x2=c(41.8417,-88.8507)
#x3=c(40.0840,-88.2404)
#x4=c(37.4368,-88.6674)
#ob_coors=rbind(ob_coors,x1,x2,x3,x4)
#read in ob site info
obs = read.table("../ICN-WARM/station_meta")
ob_coors = obs[,3:4]
ob_coors[,2] = -ob_coors[,2]
years=1980:1987

NARR_dir = "/glade/u/home/yufenghe/scratch/NARR/forcing/"
NARRsoil_dir = "~/gladework/NARR/NARR_XWLIQ_daily.nc"
#---------------------double check csspout folder!
#csspout_dir="~/gladework/csspout_test/"
#csspout_dirs=c("csspout_ctl_f4","csspout_bd13_z15_f2","csspout_bd13_z15_f4","csspout_noXumDeb")
#csspout_dirs=args
csspout_dirs="CTL_post_BD13"
cpost_file_prefix = "CTL_post_BD13"
for (foldername in csspout_dirs){
outname_mat = paste("swc_",foldername,".mat",sep="")
csspout_dir=paste("~/scratch/",csspout_dirs,"/",sep="")
nrow=195
ncol=138
#xwliq_1m_narr_cssp = array(0,c(nrow,ncol,7,12,length(years))) 
#fit3 <- lm(y~poly(x,3,raw=TRUE))
#ET
slh=2300 #J/g
#f=nc_open(paste(csspout_dir,csspout_dirs,"_ALFX_monthly.nc",sep=""))
#et_month=ncvar_get(f,"ALFX")*24*60*60*1e-3/slh
#nc_close(f)
#et_month=et_month[,,,5:(5+length(years)-1)]
#PRAVG
#f=nc_open(paste(csspout_dir,csspout_dirs,"_PRAVG_monthly.nc",sep=""))
#pr_month=ncvar_get(f,"PRAVG")
#nc_close(f)
#pr_month=pr_month[,,,5:(5+length(years)-1)]
#RUNOFF SUR 
#f=nc_open(paste(csspout_dir,csspout_dirs,"_XRSUR_monthly.nc",sep=""))
#rsur_month=ncvar_get(f,"XRSUR")
#nc_close(f)
#rsur_month=rsur_month[,,,5:(5+length(years)-1)]
#RUNOFF DRN 
#f=nc_open(paste(csspout_dir,csspout_dirs,"_XRDRN_monthly.nc",sep=""))
#rdrn_month=ncvar_get(f,"XRDRN")
#nc_close(f)
#rdrn_month=rdrn_month[,,,5:(5+length(years)-1)]
#water
#f=nc_open(paste(csspout_dir,cpost_file_prefix,"_XWLIQ_monthly.nc",sep=""))
#xwliq_month=ncvar_get(f,"XWLIQ")
#nc_close(f)
#cpost xwliq starts from 1979
#year_start_index=2
#xwliq_month=xwliq_month[,,6:14,,year_start_index:(year_start_index+length(years)-1)]
#print(c("cpost xwliq month has dimension: ",dim(xwliq_month)))
#ice
#f=nc_open(paste(csspout_dir,cpost_file_prefix,"_XWICE_monthly.nc",sep=""))
#xwice_month=ncvar_get(f,"XWICE")
#nc_close(f)
#xwice_month=xwice_month[,,6:14,,year_start_index:(year_start_index+length(years)-1)]
#}
#swc_month = array(0,c(nrow,ncol,9,12,length(years)))
#sic_month = array(0,c(nrow,ncol,9,12,length(years)))
#for (year in 1:length(years)){
#    for (month in 1:12){
#      swc_month[,,,month,year] = xwliq_month[,,,month,year]/(dz*1000)
#      sic_month[,,,month,year] = xwice_month[,,,month,year]/(dz*917)
#    }
}
swc_cwrf= 0
sic_cwrf= 0
sm_cwrf_chao= 0
et_cwrf = 0
pr_cwrf = 0
rsur_cwrf = 0
rdrn_cwrf = 0
#f=nc_open(paste("./CTL_post/CTL_xsmtg_monthly.nc",sep=""))
f=nc_open(paste("~/scratch/run_newcontrol/cpost_ctl_wrfbio/cpost_ctl_wrfbio_xsmtg_monthly.nc",sep=""))
sm_month=ncvar_get(f,"xsmtg")
nc_close(f)
for (i in 1:nrow(ob_coors)){
f<-(lat-ob_coors[i,1])^2+(lon-ob_coors[i,2])^2
f_min<-which(f==min(f),arr.ind=TRUE)
print(f_min)
#swc_cwrf = swc_cwrf + swc_month[f_min[1],f_min[2],,,]
#sic_cwrf = sic_cwrf + sic_month[f_min[1],f_min[2],,,]
sm_cwrf_chao = sm_cwrf_chao + sm_month[f_min[1],f_min[2],,,]
#et_cwrf = et_cwrf + et_month[f_min[1],f_min[2],,]
#pr_cwrf = pr_cwrf + pr_month[f_min[1],f_min[2],,]
#rsur_cwrf = rsur_cwrf + rsur_month[f_min[1],f_min[2],,]
#rdrn_cwrf = rdrn_cwrf + rdrn_month[f_min[1],f_min[2],,]
}
#swc_cwrf=swc_cwrf/nrow(ob_coors)
#sic_cwrf=sic_cwrf/nrow(ob_coors)
#et_cwrf=et_cwrf/nrow(ob_coors)
#pr_cwrf=pr_cwrf/nrow(ob_coors)
#rsur_cwrf=rsur_cwrf/nrow(ob_coors)
#rdrn_cwrf=rdrn_cwrf/nrow(ob_coors)
sm_cwrf_chao = sm_cwrf_chao/nrow(ob_coors)
#writeMat(outname_mat,swc_cwrf=swc_cwrf,sic_cwrf=sic_cwrf,et_cwrf=et_cwrf,pr_cwrf=pr_cwrf,
#         pe_cwrf=pr_cwrf-et_cwrf,rsur_cwrf=rsur_cwrf,rdrn_cwrf=rdrn_cwrf,sm_cwrf_chao=sm_cwrf_chao)
writeMat(outname_mat,sm_cwrf_chao=sm_cwrf_chao)
