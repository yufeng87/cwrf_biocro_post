library(ncdf4)
slh=2300 #J/g
f=nc_open("~/scratch/cwrfgo_biocro/BIO/BIO_ALFX_daily.nc")
alfx_bio=ncvar_get(f,"ALFX")*24*60*60*1e-3/slh
nc_close(f)
f=nc_open("~/scratch/cwrfgo_biocro/BIO/BIO_PR_daily.nc")
pr_bio=ncvar_get(f,"PRAVG")
nc_close(f)
#plot(pr_bio[115,62,],type="l",col="red")
#lines(alfx_bio[115,62,],col="green")
pe_bio=pr_bio-alfx_bio
pe_bio=pe_bio[,,c(31:dim(pe_bio)[3])]
f=nc_open("~/scratch/cwrfgo_ctl/ctl/ctl_ALFX_daily.nc")
alfx_ctl=ncvar_get(f,"ALFX")*24*60*60*1e-3/slh
nc_close(f)
f=nc_open("~/scratch/cwrfgo_ctl/ctl/ctl_PR_daily.nc")
pr_ctl=ncvar_get(f,"PRAVG")
nc_close(f)
pe_ctl=pr_ctl-alfx_ctl
pe_ctl=pe_ctl[,,c(31:dim(pe_ctl)[3])]

f=nc_open("~/MAIZE.nc")
corn=ncvar_get(f,"cor")
nc_close(f)
f=nc_open("~/cotton.nc")
cott=ncvar_get(f,"cotton")
nc_close(f)
pe_bio_year=array(0,c(195,138,8))
pe_ctl_year=pe_bio_year
ind=0
for (i in 2000:2007){
   if (i%%4==0){
        ndays=366
    }else{
        ndays=365
    }
ind=seq((ind[length(ind)]+1),(ind[length(ind)]+ndays),by=1)
tmp=pe_bio[,,ind]
pe_bio_year[,,i-1999]=rowSums(tmp,dims=2)

tmp=pe_ctl[,,ind]
pe_ctl_year[,,i-1999]=rowSums(tmp,dims=2)

}
pe_bio=rowMeans(pe_bio_year,dims=2)
pe_ctl=rowMeans(pe_ctl_year,dims=2)
state="Illinois"
result1=readRDS("result_2000_bio.rds")
lat=result1$lat
lon=result1$lon
mlu=result1$mlu
if(state=="mlu"){ 
ind=which(mlu > 0.1, arr.ind = TRUE)
}else{
source("state_yields.R")
pts_in_state=state_yields(c(lon),c(lat),state,0)
ind=arrayInd(pts_in_state,dim(lat))
}
#pe_bio_corn=mean(pe_bio[is.na(corn)==0])
#pe_ctl_corn=mean(pe_ctl[is.na(corn)==0])
#pe_bio_cott=mean(pe_bio[is.na(cott)==0])
#pe_ctl_cott=mean(pe_ctl[is.na(cott)==0])
#print(c(pe_bio_corn,pe_ctl_corn,pe_bio_cott,pe_ctl_cott))
pe_bio_state=mean(pe_bio[ind[,1],ind[,2]])
pe_ctl_state=mean(pe_ctl[ind[,1],ind[,2]])
print(c(pe_bio_state,pe_ctl_state))
