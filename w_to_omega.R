#Convert vertical velocity with units (m/s) to Pa/s.
w2omega <-  function(t,p,w){      # t: K; p: Pa; w: m/s
if(any(dim(t)!=dim(w))) stop("dim of t and w are not equal!")
       rgas  = 287.058            # J/(kg-K) => m2/(s2 K)
       g     = 9.80665            # m/s2
if(length(dim(p)) == length(dim(t))){
       rho   = p/(rgas*t)         # density => kg/m3
       omega = w*rho*g           # array operation
}else if(length(dim(t))==5){      #seasonal t
       print("helloha")
       omega = array(NaN,dim(t))
       for (i in 1:length(p)){
          pp = p[i]
          rho   = pp/(rgas*t[,,i,,])
          omega[,,i,,] = w[,,i,,]*rho*g
       }
}else{
     stop("check input dimension!")
}
       return(-omega)
}  
